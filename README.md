# vm-concierge

### What we wanted to achieve:

* Implement a system that will allow your clients to check-out and check-in VMs and help you administer usage of VMs in your cloud.
* When clients need a VM for use, they will call a checkout method provided by your VM reservation system. They should get the IP of a VM they can use, along with any other details you may need.
* When clients are done using the VM, they will call a checkin method provided by your VM reservation system.
* For the sake of this assignment, you can assume cleanup of a VM means that your system will ssh into the VM using its IP and clean up all files in the /tmp directory.
* After a VM has been cleaned up, your system should add it back to the pool of VMs available for checkout by clients.
* If a client requests a VM and no VM is available to be checked out, then your system should let your clients know accordingly, so they may retry after some time.
* The same VM cannot be checked out by two clients at the same time.
* A VM checked out by one client cannot be checked in by some other client.
* If your system stops running for some reason and needs to be restarted, then it should continue to know all the information about VMs that have been already checked out and VMs that are available.

### Potential ways:

#### Webservice:
Create a webservice which would store the inventory in a db to persist data and let the webservice handle the complexity to manage bookings and releases of the VM's

#### Custom Gitops type solution:

[Image](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Untitled%20Diagram.drawio#R7VldU6MwFP01fbQDpIH6qNR1O9MdO%2Fqw%2BhggQtZAmBBaur9%2BkxK%2BW3XX1rqOT%2BUebj445%2BbmJh0BNy6uOUqjHyzAdGQZQTECs5FlmcBx5I9CNiXinNslEHISaKcGuCO%2FsQYNjeYkwFnHUTBGBUm7oM%2BSBPuigyHO2brr9shod9QUhXgA3PmIDtGfJBBRiU6h0eDfMQmjamTT0G9iVDlrIItQwNYtCFyNgMsZE%2BVTXLiYKvIqXsp23%2Fa8rSfGcSJe0yCbhwvo3TwYa%2BHCArmLi01xBvTcxKb6YBzI79cm4yJiIUsQvWrQS87yJMCqV0Najc%2BCsVSCpgR%2FYSE2WkyUCyahSMRUv5UT5pt73X5rPChjDCtzVrRfzjbaKueqJriXAg1lLOc%2Bfua7q1BCPMTiGT%2BrFkpGOGYxlvOR7TimSJBVdx5Ih1pY%2BzVqyActyF%2BIo%2FtdIZrrkS58wfhAMhlZqXrMY1o6gMsV5oLIKF4gD9Mly4ggLJEuHhOCxS2HC0pC9UIo7doisVxQkmC3XleNAqotLp7XYMiZbgD0ctD5wIKwtNfN6qpcotbCso0jkTw5xQo4YCS%2FNUJ10yUjcuRaJWvak2kKu12UK0e36klQT%2BPfVbEGoe8u5gOlGh0UqeuICHyXoi1da7kd7eL8TbFbJ%2FeKlSoqW8Fb%2B7SjFx4res2ThC8uiLhvPZfp24LabNK3MqrsXWf9M2NsWLCb%2Bm3TeSH5b60l5kQSh%2FnhdwT7lTuCuSdE3mlLgB9Hb%2BeVevd2eTAF%2F4vUJ9397UEKHFk2lfO99GQJYIdiS4qNYpXnEi9Lt7bxBX1BX9CboBNXGYPay3DGwyLZdHbUGSaER8pG00E2micr%2BUFM931KwoABu4RVR%2B42XRYc0jU5Vll2PmDrFmeYr5A6hmUfgLA6oja9mOtQZo3fk7TqyqfF2jURFHkSu8Upe3GhSkJEl6tMcPYkz7BUHYpnCUtUOfRIKO1BSJ%2BCfUkgbp%2Bfq%2BNxTIJgW0vtEqdbXx1Cn94hGQBrRwY435EAwNHEGV5GLDlO1fAsEzKkjTwjSaiGTjLiSa723VKknPk4y14OdA%2F5T%2BGW25vyLuJwCwBOeinW3BX%2B73kNYe448VKMkvyzUAyMk1M8rKjr%2FOLOZZfu7POmlIndi3hnKId9mIQizeZWu7wQav4bAFd%2FAA%3D%3D)

### Why choose the Custom gitops solution

* is simple to mantain
* extremely resource lite 
* gives an out of the box audit log in form of commits 
* is easily recoverable
* keeps all the complexity of handling infra at one place

### Whats has not been achieved 

* vm locking (can explain the posisble workflows over the call may be)

### Where can we go from here

The code has been designed to keep furture pivots to a need for webservic ein mind and hence the cli which could have been a shell script has been written in python witha decent class structure which can help pivot us to the webservice route. 

A lot of nitty-gritty around security hardening and distribution are yet to be completed. This is quite raw in that perspective. 

### Usage:

#### actions: 

        list:       list all available/reserved hosts
        reserve:    reserve a host
        release:    release a hostname
        
#### options

        -d/--datacenter:    datacenter to choose the host from 
        -c/--category:      category of the host to choose
        -r/--reserved:      when used ith "list" action renders reserved hosts by the user
        -h/--hostname:      to be used with "release" to release a hostname
        -w/--with-roles:    allows to apply roles to the host at the time of reserving   
    


