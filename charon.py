import sys
import getopt
from concierge import Concierge


def usage():
    print("usage")
    print("actions: ")
    print("""
        list:       list all available/reserved hosts
        reserve:    reserve a host
        release:    release a hostname
        """)

    print("options")
    print("""
        -d/--datacenter:    datacenter to choose the host from 
        -c/--category:      category of the host to choose
        -r/--reserved:      when used ith "list" action renders reserved hosts by the user
        -h/--hostname:      to be used with "release" to release a hostname
        -w/--with-roles:    allows to apply roles to the host at the time of reserving   
    """)

    exit(1)


def parse_options():

    reserved = None
    action = sys.argv[1]
    argv = sys.argv[2:]

    try:
        opts, args = getopt.getopt(argv, "d:c:h:w:r", [
            "datacenter=",
            "category=",
            "hostname=",
            "with-roles=",
            "reserved"
        ])
    except:
        usage()

    for opt, arg in opts:
        if opt in ['-d', '--datacenter']:
            concierge.set_datacenter(arg)
        elif opt in ['-c', '--category']:
            concierge.set_category(arg)
        elif opt in ['-h', '--hostname']:
            concierge.set_hostname(arg)
        elif opt in ['-r', '--reserved']:
            reserved = "true"
        elif opt in ['-w', '--with-roles']:
            concierge.set_roles(arg)
        else:
            usage()

    if action == "list":
        if concierge.datacenter is None and reserved is None:
            usage()
        if concierge.category is None and reserved is None:
            usage()
        if reserved is not None:
            concierge.set_datacenter("all")
            concierge.get_reserved_hosts()
        else:
            concierge.get_available_hosts()
    elif action == "reserve":
        if concierge.category is None:
            usage()
        if concierge.datacenter is None:
            usage()
        concierge.checkin()
    elif action == "release":
        if concierge.hostname is None:
            usage()
        concierge.set_datacenter("all")
        concierge.checkout()
    else:
        usage()


concierge = Concierge.Concierge()
parse_options()
