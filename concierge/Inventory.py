import git
import yaml
import os
import subprocess


class Inventory:

    base_inventory_path = "inventory/"
    repository = os.getenv("GIT_HOST") + "/" + os.getenv("INVENTORY_REPO")
    git_cache_dir = os.getenv("REPO_CACHE_DIR")

    def __init__(self, dc):

        self.category = None

        self.dc = dc

        if os.name == 'nt':
            subprocess.check_output(['cmd', '/C', 'rmdir', '/S', '/Q', os.path.abspath(self.git_cache_dir)])
        else:
            subprocess.check_output(['rm', '-rf', os.path.abspath(self.git_cache_dir)])

        self.repo = git.Repo.clone_from(self.repository + ".git", self.git_cache_dir, branch="main")

        if self.dc != "all":
            self.inventory = self.git_cache_dir + "/" + self.base_inventory_path + "/" + self.dc + ".yml"
            with open(self.inventory, "r") as stream:
                try:
                    self.inventory = yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)

    def refresh_inventory(self):

        self.repo.git.refresh(self.git_cache_dir)

    def get_host_by_category(self, category):

        self.category = category
        for inventory_category in self.inventory["all"]["children"]:
            if self.category == inventory_category:
                return self.inventory["all"]["children"][inventory_category]["hosts"]

    def get_reserved_hosts(self):

        return os.listdir(self.git_cache_dir+"/"+".reserved")

    def get_reserved_hosts_user(self, user):

        reserved_hosts = []
        for reserved_host in os.listdir(self.git_cache_dir+"/"+".reserved"):
            with open(self.git_cache_dir+"/"+".reserved/"+reserved_host, "r") as reserved_file:
                for line in reserved_file.readlines():
                    if "Reserved to:"+user in line:
                        reserved_hosts.extend([reserved_host])

        return reserved_hosts

    def get_reservation_details(self, hostname):

        details = {}
        with open(self.git_cache_dir + "/" + ".reserved/" + hostname, "r") as reserved_file:
            for line in reserved_file.readlines():
                if "Reserved to:" in line:
                    details["reserved_for"] = line.split(":")[1].replace('\n', '')
                elif "Reserved On:" in line:
                    details["reserved_on"] = line.split(":")[1].replace('\n', '')
                elif "Datacenter:" in line:
                    details["datacenter"] = line.split(":")[1].replace('\n', '')
                elif "Category:" in line:
                    details["category"] = line.split(":")[1].replace('\n', '')

        return details
