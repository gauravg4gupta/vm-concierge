import gitlab
import os
from concierge import Inventory
from datetime import datetime


class Concierge:

    max_book_host_by_category = 1

    def __init__(self):

        self.datacenter = None
        self.inventory = None
        self.category = None
        self.hostname = None
        self.roles = None
        self.operation = None

        self.repository = os.getenv("INVENTORY_REPO")
        self.private_token = os.getenv("ACCESS_TOKEN")

        self.gl = gitlab.Gitlab(private_token=self.private_token)
        self.gl.auth()
        self.user = self.gl.user
        self.project = self.gl.projects.get(self.repository)

        print("Welcome to the continental grounds "+self.user.name)

    def set_datacenter(self, datacenter):

        self.datacenter = datacenter
        self.inventory = Inventory.Inventory(self.datacenter)

    def set_category(self, category):

        self.category = category

    def set_hostname(self, hostname):

        self.hostname = hostname

    def set_roles(self, roles):

        self.roles = roles

    def set_operation(self, operation):

        self.operation = operation

    def checkin(self):

        available_hosts = self.get_available_hosts()
        print("Reserving "+available_hosts[0]+" for you")
        self.set_hostname(available_hosts[0])

        reserve_data = open("reserve_host.tmp", "w")
        reserve_data_lines = [
            "Reserved to:"+self.user.name+"\n",
            "Reserved On:"+str(datetime.now())+"\n",
            "Datacenter:"+self.datacenter+"\n",
            "Category:"+self.category+"\n"
        ]
        reserve_data.writelines(reserve_data_lines)
        reserve_data.close()
        data = {
            'branch': 'main',
            'commit_message': '[concierge action:reserve] for '+self.user.name+' [host:'+self.hostname+']',
            'actions': [
                {
                    'action': 'create',
                    'file_path': '.reserved/'+self.hostname,
                    'content': open('reserve_host.tmp').read(),
                }
            ]
        }
        try:
            self.project.commits.create(data)
        except Exception as exc:
            print(exc)
        os.remove("reserve_host.tmp")

        if self.roles is not None:
            self.set_operation('prep')
            self.run_task()

    def checkout(self):

        data = {
            'branch': 'main',
            'commit_message': '[concierge action:release] for ' + self.user.name + ' [host:' + self.hostname + ']',
            'actions': [
                {
                    'action': 'delete',
                    'file_path': '.reserved/' + self.hostname,
                }
            ]
        }
        try:
            self.project.commits.create(data)
        except Exception as exc:
            print(exc)
        details = self.inventory.get_reservation_details(self.hostname)
        self.set_datacenter(details["datacenter"])
        self.set_category(details["category"])
        self.housekeep()

    def get_available_hosts(self):

        available_hosts = []
        hosts = self.inventory.get_host_by_category(self.category)
        print(str(len(hosts)) + " hosts available in the category ...  Checking availability")
        reserved_hosts = self.inventory.get_reserved_hosts()
        print("{:<55} {:<8}".format('Hostname', 'Zone'))
        for host, prop in hosts.items():
            if host not in reserved_hosts:
                print("{:<55} {:<8}".format(host, prop["zone"]))
                available_hosts.extend([host])

        return available_hosts

    def get_reserved_hosts(self):

        reserved_hosts = self.inventory.get_reserved_hosts_user(self.user.name)
        print("Hosts reserved by you are:")
        for host in reserved_hosts:
            print(host)

    def housekeep(self):

        self.set_operation('cleanup')
        self.run_task()

    def run_task(self):

        data = {
            'ref': 'main',
            'variables': [
                {'key': 'HOST', 'value': self.hostname},
                {'key': 'DC', 'value': self.datacenter},
                {'key': 'HOST_GROUP', 'value': self.category},
                {'key': 'OPERATIONS', 'value': self.operation}
            ]
        }

        self.project.pipelines.create(data)
